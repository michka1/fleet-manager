# Fleet Manager

Fleet Manager is a fleet management app, where you can create fleets, register vehicles in them and park each vehicle
in gps-based coordinates. It's mainly a DDD-CQRS exercise with BDD tests embedded.

## Installation

```bash
composer install
cp .env.example .env
# fill the .env variables 
# if you want to launch Behat tests, APP_ENV=test and provide a name for DB_TEST_NAME
php db-migration.php
```

In order to launch tests:
```shell
# Behat
composer test:behat
# Unit tests
composer test:phpunit
```

## Usage

```shell
# <userId> being a valid UUID
php ./fleet create <userId> # returns fleetId on the standard output

# <fleetId> being a valid existing UUID (returned by the previous command for example)
# <vehiclePlateNumber> being a valid french SIV
php ./fleet register-vehicle <fleetId> <vehiclePlateNumber>

# <vehiclePlateNumber> being an existing SIV id in fleet <fleetId>
./fleet park-vehicle <fleetId> <vehiclePlateNumber> <lat> <lng> [alt]

# <vehiclePlateNumber> being an existing SIV id in fleet <fleetId>
./fleet localize-vehicle <fleetId> <vehiclePlateNumber> # returns coordinates on the standard output
```

## Notes

### Code quality

For helping to maintain a high level in code quality, I could (or do) use:
  * Linters: 
    * PHP CS Fixer, PHP Code Sniffer, Laravel Pint... (standards compliance)
    * PHPStan, Psalm (static analysis)
  * Testing obviously
    * unit tests 
    * functional 
    * end to end... 
    * here I use unit testing with PHPUnit and BDD features testing with Behat
  * other libraries like:
    * Sentry for error tracking and performance monitoring
    * Sonarqube, Qodana, ... for clean code standards, code smells like duplication, nesting complexity...
    * Blackfire for monitoring, profiling, performance testing
    * ...

### CI/CD

An example of simple CI with gitlab is provided within the project