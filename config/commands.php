<?php

return [
    Fulll\App\Fleet\Command\CreateFleet::class => Fulll\App\Fleet\Command\CreateFleetHandler::class,
    Fulll\App\Fleet\Command\RegisterVehicleIntoFleet::class => Fulll\App\Fleet\Command\RegisterVehicleIntoFleetHandler::class,
    Fulll\App\Fleet\Command\ParkVehicle::class => Fulll\App\Fleet\Command\ParkVehicleHandler::class
];
