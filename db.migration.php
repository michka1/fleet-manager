<?php
function getSql($dbName)
{
    return
        <<<SQL
            CREATE DATABASE IF NOT EXISTS $dbName;
            USE $dbName;
            
            CREATE TABLE IF NOT EXISTS fleet (
                id CHAR(36) PRIMARY KEY,
                user_id CHAR(36) NOT NULL,
                name VARCHAR(50)
            );
            
            CREATE TABLE IF NOT EXISTS vehicle (
                id CHAR(36) PRIMARY KEY,
                plate_number VARCHAR(15) UNIQUE NOT NULL,
                latitude DECIMAL(8, 6),
                longitude DECIMAL(9, 6),
                altitude INT DEFAULT 0
            );
            
            CREATE TABLE IF NOT EXISTS fleet_vehicle (
                fleet_id CHAR(36),
                vehicle_id CHAR(36),
                PRIMARY KEY (fleet_id, vehicle_id),
                FOREIGN KEY (fleet_id) REFERENCES fleet(id) ON DELETE CASCADE,
                FOREIGN KEY (vehicle_id) REFERENCES vehicle(id) ON DELETE CASCADE
            );
            SQL
        ;
}

require __DIR__ . '/vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$host = $_ENV['DB_HOST'];
$username = $_ENV['DB_USER'];
$password = $_ENV['DB_PASSWORD'];

foreach ([$_ENV['DB_NAME'], $_ENV['DB_TEST_NAME']] as $dbName) {
    if ($dbName=='') {
        continue;
    }

    $sql = getSql($dbName);

    try {
        $pdo = new PDO("mysql:host=$host", $username, $password);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $pdo->exec($sql);

        echo "Databases successfully created.\n";
    } catch (PDOException $e) {
        echo "Error while creating databases: " . $e->getMessage() . "\n";
        exit(1);
    }
}
