<?php

declare(strict_types=1);

use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Hook\AfterScenario;
use Behat\Hook\BeforeScenario;
use Behat\Step\Given;
use Behat\Step\Then;
use Behat\Step\When;
use Dotenv\Dotenv;
use Fulll\App\Fleet\FleetService;
use Fulll\Bootloader\Bootstrap;
use Fulll\Domain\Fleet\Characteristics\FleetId;
use Fulll\Domain\Fleet\Characteristics\UserId;
use Fulll\Domain\Fleet\Exception\DuplicateVehicleException;
use Fulll\Domain\Fleet\Exception\VehicleNotParkedException;
use Fulll\Domain\Fleet\Repository\FleetRepository;
use Fulll\Domain\Shared\Exception\InvalidIdException;
use Fulll\Domain\Vehicle\Characteristics\Location;
use Fulll\Domain\Vehicle\Characteristics\PlateNumber;
use Fulll\Domain\Vehicle\Characteristics\VehicleId;
use Fulll\Domain\Vehicle\Exception\InvalidCoordinatesException;
use Fulll\Domain\Vehicle\Exception\InvalidPlateNumberException;
use Fulll\Domain\Vehicle\Repository\VehicleRepository;
use Fulll\Domain\Vehicle\Vehicle;
use Fulll\Infra\DbConnection;
use PHPUnit\Framework\Assert;

class FeatureContext implements Context
{
    private const REPOSITORY_TYPE_PDO = 'pdo';
    private const REPOSITORY_TYPE_INMEMORY = 'inMemory';
    private string $fleetId;
    private string $otherFleetId;
    private ?Exception $exception = null;
    private FleetRepository $fleetRepository;
    private VehicleRepository $vehicleRepository;
    private FleetService $fleetService;
    private string $plateNumber;
    /**
     * @var array{
     *     latitude: float,
     *     longitude: float,
     *     altitude?: int
     * }
     */
    private array $location;
    /**
     * @var array{
     *     latitude: float,
     *     longitude: float,
     *     altitude?: int
     * }
     */
    private array $myVehicleLocation;
    private DbConnection $dbConnection;

    #[BeforeScenario]
    public function initializeContext(BeforeScenarioScope $scope): void
    {
        $dotenv = Dotenv::createImmutable(__DIR__ . '/../../', '.env');
        $dotenv->load();

        if (($_ENV['APP_ENV'] ?? null) !== 'test') {
            throw new Exception('You\'re not in a testing environment!');
        }

        $tags = $scope->getScenario()->getTags();
        $_ENV['REPOSITORY_TYPE'] = in_array('critical', $tags)
            ? self::REPOSITORY_TYPE_PDO
            : self::REPOSITORY_TYPE_INMEMORY
        ;

        $app = Bootstrap::run();

        $container = $app->loadServiceContainer();

        $this->fleetService = $container->get(FleetService::class);
        $this->fleetRepository = $container->get(FleetRepository::class);
        $this->vehicleRepository = $container->get(VehicleRepository::class);
        $this->dbConnection = $container->get(DbConnection::class);

        $this->dbConnection->beginTransaction();
    }

    #[AfterScenario]
    public function rollbackTransactions(): void
    {
        $this->dbConnection->rollBack();
    }

    #[Given('my fleet')]
    public function myFleet(): void
    {
        $this->fleetId = $this->fleetService->createFleet(UserId::generate()->toNative(), 'My shiny fleet');
    }

    /**
     * @throws InvalidPlateNumberException
     * @throws InvalidIdException
     */
    #[Given('a vehicle')]
    public function aVehicle(): void
    {
        $this->plateNumber = 'AA-123-BB';
        $vehicle = new Vehicle(VehicleId::generate(), PlateNumber::fromNative($this->plateNumber));
        $this->vehicleRepository->create($vehicle);
    }

    #[
        When('I register this vehicle into my fleet'),
        When('I have registered this vehicle into my fleet'),
        When('I try to register this vehicle into my fleet'),
    ]
    public function iRegisterThisVehicleIntoMyFleet(): void
    {
        $vehicle = $this->vehicleRepository->findByPlateNumber(PlateNumber::fromNative($this->plateNumber));

        try {
            $this->fleetService->registerVehicleIntoFleet(
                $this->fleetId,
                $vehicle->plateNumber()->toNative()
            );
        } catch (Exception $e) {
            $this->exception = $e;
        }
    }

    #[Then('this vehicle should be a part of my vehicle fleet')]
    public function thisVehicleShouldBeAPartOfMyVehicleFleet(): void
    {
        $fleet = $this->fleetRepository->findById(FleetId::fromNative($this->fleetId));
        $vehicle = $this->vehicleRepository->findByPlateNumber(PlateNumber::fromNative($this->plateNumber));

        Assert::assertTrue($fleet->contains($vehicle));
    }

    /**
     * @throws Exception
     */
    #[Then('I should be informed that this vehicle is already a part of my fleet')]
    public function iShouldBeInformedThatThisVehicleIsAlreadyAPartOfMyFleet(): void
    {
        Assert::assertInstanceOf(
            DuplicateVehicleException::class,
            $this->exception,
            "Expected DuplicateVehicleException, got a different exception or no exception."
        );

        Assert::assertEquals(
            'Vehicle with plate number: "' . $this->plateNumber . '" already belongs to the fleet',
            $this->exception->getMessage()
        );
    }

    #[Given('the fleet of another user')]
    public function theFleetOfAnotherUser(): void
    {
        $this->otherFleetId = $this->fleetService->createFleet(UserId::generate()->toNative(), 'another fleet');
    }

    /**
     * @throws Exception
     */
    #[Given('this vehicle has been registered into another fleet')]
    public function thisVehicleHasBeenRegisteredIntoAnotherFleet(): void
    {
        $vehicle = $this->vehicleRepository->findByPlateNumber(PlateNumber::fromNative($this->plateNumber));
        $this->fleetService->registerVehicleIntoFleet($this->otherFleetId, $vehicle->plateNumber()->toNative());
    }

    #[Given('a location')]
    public function aLocation(): void
    {
        $this->location = [
            'latitude' => 5.245845,
            'longitude' => 68.24,
            'altitude' => 10
        ];
    }

    #[
        When('I park my vehicle at this location'),
        When('I try to park my vehicle at this location'),
    ]
    public function iParkMyVehicleAtThisLocation(): void
    {
        $vehicle = $this->vehicleRepository->findByPlateNumber(PlateNumber::fromNative($this->plateNumber));

        try {
            $this->fleetService->parkVehicleTolocation(
                $this->fleetId,
                $vehicle->plateNumber()->toNative(),
                ...$this->location
            );
        } catch (Exception $e) {
            $this->exception = $e;
        }
    }

    #[Given('my vehicle has been parked into this location')]
    public function myVehicleHasBeenParkedIntoThisLocation(): void
    {
        $vehicle = $this->vehicleRepository->findByPlateNumber(PlateNumber::fromNative($this->plateNumber));

        $this->fleetService->parkVehicleTolocation(
            $this->fleetId,
            $vehicle->plateNumber()->toNative(),
            ...$this->location
        );
    }

    #[Then('the known location of my vehicle should verify this location')]
    public function theKnownLocationOfMyVehicleShouldVerifyThisLocation(): void
    {
        $vehicle = $this->vehicleRepository->findByPlateNumber(PlateNumber::fromNative($this->plateNumber));

        Assert::assertTrue(
            $vehicle->location()->equals(
                Location::fromNative(...$this->location)
            )
        );
    }

    #[Then('I should be informed that my vehicle is already parked at this location')]
    public function iShouldBeInformedThatMyVehicleIsAlreadyParkedAtThisLocation(): void
    {
        Assert::assertInstanceOf(
            InvalidCoordinatesException::class,
            $this->exception,
            "Expected InvalidCoordinatesException, got a different exception or no exception."
        );

        Assert::assertEquals(
            'Invalid coordinates provided: this vehicle was already parked to this location.',
            $this->exception->getMessage()
        );
    }

    #[When('I try to localize my vehicle')]
    public function iTryToLocalizeMyVehicle(): void
    {
        try {
            $this->myVehicleLocation = $this->fleetService->localizeVehicle($this->fleetId, $this->plateNumber);
        } catch (VehicleNotParkedException $e) {
            $this->exception = $e;
        }
    }

    #[Then('I should receive the valid coordinates')]
    public function iShouldReceiveTheValidCoordinates(): void
    {
        Assert::assertEquals($this->location, $this->myVehicleLocation);
    }

    #[Then('I should be informed that my vehicle has not been parked yet')]
    public function iShouldBeInformedThatMyVehicleHasNotBeenParkedYet(): void
    {
        Assert::assertInstanceOf(
            VehicleNotParkedException::class,
            $this->exception,
            "Expected VehicleNotParkedException, got a different exception or no exception."
        );

        Assert::assertEquals(
            'Vehicle ' . $this->plateNumber . ' is not parked anywhere',
            $this->exception->getMessage()
        );
    }
}
