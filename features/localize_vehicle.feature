Feature: Park a vehicle

  In order to manage my vehicles
  As an application user
  I should be able to localize my vehicle location

  Background:
    Given my fleet
    And a vehicle
    And I have registered this vehicle into my fleet

  @critical
  Scenario: Successfully localize my vehicle
    And a location
    And my vehicle has been parked into this location
    When I try to localize my vehicle
    Then I should receive the valid coordinates

  Scenario: Can't localize a vehicle never parked before
    When I try to localize my vehicle
    Then I should be informed that my vehicle has not been parked yet