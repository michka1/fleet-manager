<?php

namespace Fulll\App\Contracts;

interface MessageBusInterface
{
    public function registerHandler(string $messageName, MessageHandler $handler): self;

    public function send(Message $message): mixed;
}
