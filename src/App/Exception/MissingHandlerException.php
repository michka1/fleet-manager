<?php

namespace Fulll\App\Exception;

use Exception;

class MissingHandlerException extends Exception
{
    public function __construct(string $commandName = null)
    {
        $message = 'Couldn\'t find handler for command: ' . $commandName;

        parent::__construct($message);
    }
}
