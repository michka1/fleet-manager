<?php

namespace Fulll\App\Fleet\Command;

use Fulll\App\Contracts\Message;
use Fulll\Domain\Fleet\Characteristics\FleetId;
use Fulll\Domain\Fleet\Characteristics\UserId;
use Fulll\Domain\Shared\Exception\InvalidIdException;

class CreateFleet implements Message
{
    public function __construct(
        private readonly UserId $userId,
        private readonly ?string $name = null,
        private ?FleetId $id = null
    ) {
    }

    /**
     * @throws InvalidIdException
     */
    public function id(): FleetId
    {
        if (is_null($this->id)) {
            $this->id = FleetId::generate();
        }

        return $this->id;
    }

    public function name(): ?string
    {
        return $this->name;
    }

    public function userId(): UserId
    {
        return $this->userId;
    }
}
