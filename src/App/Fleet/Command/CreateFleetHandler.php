<?php

namespace Fulll\App\Fleet\Command;

use Fulll\App\Contracts\MessageHandler;
use Fulll\Domain\Fleet\Fleet;
use Fulll\Domain\Fleet\Repository\FleetRepository;
use Fulll\Domain\Shared\Exception\InvalidIdException;

final class CreateFleetHandler implements MessageHandler
{
    public function __construct(private readonly FleetRepository $repository)
    {
    }

    /**
     * @throws InvalidIdException
     */
    public function handle(CreateFleet $createFleetCommand): Fleet
    {
        $fleet = new Fleet($createFleetCommand->id(), $createFleetCommand->userId(), $createFleetCommand->name());

        $this->repository->create($fleet);

        return $fleet;
    }
}
