<?php

namespace Fulll\App\Fleet\Command;

use Fulll\App\Contracts\Message;
use Fulll\Domain\Fleet\Characteristics\FleetId;
use Fulll\Domain\Vehicle\Characteristics\Location;
use Fulll\Domain\Vehicle\Characteristics\PlateNumber;

class ParkVehicle implements Message
{
    public function __construct(
        private readonly FleetId $fleetId,
        private readonly PlateNumber $plateNumber,
        private readonly Location $location
    ) {
    }

    public function fleetId(): FleetId
    {
        return $this->fleetId;
    }

    public function plateNumber(): PlateNumber
    {
        return $this->plateNumber;
    }

    public function location(): Location
    {
        return $this->location;
    }
}
