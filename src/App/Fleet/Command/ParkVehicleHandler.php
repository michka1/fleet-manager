<?php

namespace Fulll\App\Fleet\Command;

use Fulll\App\Contracts\MessageHandler;
use Fulll\App\Fleet\Exception\FleetNotFoundException;
use Fulll\App\Fleet\Exception\VehicleNotFoundException;
use Fulll\Domain\Fleet\Exception\VehicleDoesntBelongToFleetException;
use Fulll\Domain\Fleet\Repository\FleetRepository;
use Fulll\Domain\Shared\Exception\IncompatibleForEqualityCheckException;
use Fulll\Domain\Vehicle\Exception\InvalidCoordinatesException;
use Fulll\Domain\Vehicle\Repository\VehicleRepository;

class ParkVehicleHandler implements MessageHandler
{
    public function __construct(
        private readonly FleetRepository $fleetRepository,
        private readonly VehicleRepository $vehicleRepository
    ) {
    }

    /**
     * @throws FleetNotFoundException
     * @throws VehicleDoesntBelongToFleetException
     * @throws IncompatibleForEqualityCheckException
     * @throws InvalidCoordinatesException
     * @throws VehicleNotFoundException
     */
    public function handle(ParkVehicle $parkVehicleCommand): void
    {
        $fleetId = $parkVehicleCommand->fleetId();
        $fleet = $this->fleetRepository->findById($fleetId) ?? throw new FleetNotFoundException($fleetId->toNative());

        $plateNumber = $parkVehicleCommand->plateNumber();
        $vehicle = $this->vehicleRepository->findByPlateNumber($plateNumber)
            ?? throw new VehicleNotFoundException($plateNumber->toNative())
        ;

        $fleet->parkVehicle($vehicle, $parkVehicleCommand->location());

        $this->vehicleRepository->update($vehicle);
    }
}
