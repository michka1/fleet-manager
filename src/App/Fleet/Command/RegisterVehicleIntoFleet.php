<?php

namespace Fulll\App\Fleet\Command;

use Fulll\App\Contracts\Message;
use Fulll\Domain\Fleet\Characteristics\FleetId;
use Fulll\Domain\Vehicle\Characteristics\PlateNumber;

final class RegisterVehicleIntoFleet implements Message
{
    public function __construct(private readonly FleetId $fleetId, private readonly PlateNumber $plateNumber)
    {
    }

    public function fleetId(): FleetId
    {
        return $this->fleetId;
    }

    public function plateNumber(): PlateNumber
    {
        return $this->plateNumber;
    }
}
