<?php

namespace Fulll\App\Fleet\Command;

use Fulll\App\Contracts\MessageHandler;
use Fulll\App\Fleet\Exception\FleetNotFoundException;
use Fulll\Domain\Fleet\Exception\DuplicateVehicleException;
use Fulll\Domain\Fleet\Repository\FleetRepository;
use Fulll\Domain\Shared\Exception\IncompatibleForEqualityCheckException;
use Fulll\Domain\Shared\Exception\InvalidIdException;
use Fulll\Domain\Vehicle\Characteristics\PlateNumber;
use Fulll\Domain\Vehicle\Characteristics\VehicleId;
use Fulll\Domain\Vehicle\Repository\VehicleRepository;
use Fulll\Domain\Vehicle\Vehicle;

class RegisterVehicleIntoFleetHandler implements MessageHandler
{
    public function __construct(
        private readonly FleetRepository $fleetRepository,
        private readonly VehicleRepository $vehicleRepository
    ) {
    }

    /**
     * @throws DuplicateVehicleException
     * @throws FleetNotFoundException
     * @throws InvalidIdException
     * @throws IncompatibleForEqualityCheckException
     */
    public function handle(RegisterVehicleIntoFleet $registerCommand): void
    {
        $fleet = $this->fleetRepository->findById($registerCommand->fleetId());

        if (is_null($fleet)) {
            throw new FleetNotFoundException($registerCommand->fleetId()->toNative());
        }

        $vehicle = $this->vehicleRepository->findByPlateNumber($registerCommand->plateNumber());

        if (is_null($vehicle)) {
            $vehicle = $this->createVehicle($registerCommand->plateNumber());
        }

        $fleet->registerVehicle($vehicle);

        $this->fleetRepository->update($fleet);
    }

    /**
     * @throws InvalidIdException
     */
    private function createVehicle(PlateNumber $plateNumber): Vehicle
    {
        $vehicle = new Vehicle(VehicleId::generate(), $plateNumber);

        $this->vehicleRepository->create($vehicle);

        return $vehicle;
    }
}
