<?php

namespace Fulll\App\Fleet\Exception;

use Exception;

class FleetNotFoundException extends Exception
{
    public function __construct(string $fleetId)
    {
        $message = 'Couldn\'t find fleet with id: ' . $fleetId;

        parent::__construct($message);
    }
}
