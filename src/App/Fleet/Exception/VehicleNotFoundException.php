<?php

namespace Fulll\App\Fleet\Exception;

use Exception;

class VehicleNotFoundException extends Exception
{
    public function __construct(string $plateNumber)
    {
        $message = 'Couldn\'t find vehicle with plate number: ' . $plateNumber;

        parent::__construct($message);
    }
}
