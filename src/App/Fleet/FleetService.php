<?php

namespace Fulll\App\Fleet;

use Fulll\App\Contracts\MessageBusInterface;
use Fulll\App\Fleet\Command\CreateFleet;
use Fulll\App\Fleet\Command\ParkVehicle;
use Fulll\App\Fleet\Command\RegisterVehicleIntoFleet;
use Fulll\App\Fleet\Exception\FleetNotFoundException;
use Fulll\App\Fleet\Exception\VehicleNotFoundException;
use Fulll\App\Fleet\Query\LocalizeVehicle;
use Fulll\Domain\Fleet\Characteristics\FleetId;
use Fulll\Domain\Fleet\Characteristics\UserId;
use Fulll\Domain\Fleet\Exception\DuplicateVehicleException;
use Fulll\Domain\Fleet\Exception\VehicleDoesntBelongToFleetException;
use Fulll\Domain\Fleet\Exception\VehicleNotParkedException;
use Fulll\Domain\Fleet\Fleet;
use Fulll\Domain\Shared\Exception\IncompatibleForEqualityCheckException;
use Fulll\Domain\Shared\Exception\InvalidIdException;
use Fulll\Domain\Vehicle\Characteristics\Location;
use Fulll\Domain\Vehicle\Characteristics\PlateNumber;
use Fulll\Domain\Vehicle\Exception\InvalidCoordinatesException;
use Fulll\Domain\Vehicle\Exception\InvalidPlateNumberException;

class FleetService
{
    public function __construct(private readonly MessageBusInterface $messageBus)
    {
    }

    /**
     * @throws InvalidIdException
     */
    public function createFleet(string $nativeUserId, ?string $name = null): string
    {
        /** @var Fleet $fleet */
        $fleet = $this->messageBus->send(new CreateFleet(UserId::fromNative($nativeUserId), $name));

        return $fleet->id()->toNative();
    }

    /**
     * @throws InvalidIdException
     * @throws InvalidPlateNumberException
     * @throws DuplicateVehicleException
     * @throws FleetNotFoundException
     * @throws IncompatibleForEqualityCheckException
     */
    public function registerVehicleIntoFleet(string $nativeFleetId, string $nativePlateNumber): void
    {
        $fleetId = FleetId::fromNative($nativeFleetId);

        $plateNumber = PlateNumber::fromNative($nativePlateNumber);

        $this->messageBus->send(new RegisterVehicleIntoFleet($fleetId, $plateNumber));
    }

    /**
     * @throws InvalidIdException
     * @throws InvalidCoordinatesException
     * @throws FleetNotFoundException
     * @throws VehicleDoesntBelongToFleetException
     * @throws IncompatibleForEqualityCheckException
     * @throws InvalidCoordinatesException
     * @throws VehicleNotFoundException
     */
    public function parkVehicleToLocation(
        string $nativeFleetId,
        string $nativePlateNumber,
        float $latitude,
        float $longitude,
        ?int $altitude = null
    ): void {
        $parkVehicleCommand = new ParkVehicle(
            FleetId::fromNative($nativeFleetId),
            PlateNumber::fromNative($nativePlateNumber),
            Location::fromNative($latitude, $longitude, $altitude)
        );

        $this->messageBus->send($parkVehicleCommand);
    }

    /**
     * @return array{
     *     latitude: float,
     *     longitude: float,
     *     altitude: int
     * }
     *
     * @throws InvalidIdException
     * @throws VehicleDoesntBelongToFleetException
     * @throws FleetNotFoundException
     * @throws VehicleNotFoundException
     * @throws VehicleNotParkedException
     */
    public function localizeVehicle(string $nativeFleetId, string $nativePlateNumber): array
    {
        $localizeVehicleQuery = new localizeVehicle(
            FleetId::fromNative($nativeFleetId),
            PlateNumber::fromNative($nativePlateNumber),
        );

        /** @var Location $location */
        $location =  $this->messageBus->send($localizeVehicleQuery);

        return [
            'latitude' => $location->latitude(),
            'longitude' => $location->longitude(),
            'altitude' => $location->altitude()
        ];
    }
}
