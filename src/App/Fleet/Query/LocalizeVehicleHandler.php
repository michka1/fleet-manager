<?php

namespace Fulll\App\Fleet\Query;

use Fulll\App\Contracts\MessageHandler;
use Fulll\App\Fleet\Exception\FleetNotFoundException;
use Fulll\App\Fleet\Exception\VehicleNotFoundException;
use Fulll\Domain\Fleet\Exception\VehicleDoesntBelongToFleetException;
use Fulll\Domain\Fleet\Exception\VehicleNotParkedException;
use Fulll\Domain\Fleet\Repository\FleetRepository;
use Fulll\Domain\Shared\Exception\IncompatibleForEqualityCheckException;
use Fulll\Domain\Vehicle\Characteristics\Location;
use Fulll\Domain\Vehicle\Repository\VehicleRepository;

class LocalizeVehicleHandler implements MessageHandler
{
    public function __construct(
        private readonly FleetRepository $fleetRepository,
        private readonly VehicleRepository $vehicleRepository
    ) {
    }

    /**
     * @throws VehicleDoesntBelongToFleetException
     * @throws FleetNotFoundException
     * @throws VehicleNotFoundException
     * @throws VehicleNotParkedException
     * @throws IncompatibleForEqualityCheckException
     */
    public function handle(LocalizeVehicle $localizeVehicleQuery): Location
    {
        $fleetId = $localizeVehicleQuery->fleetId();
        $fleet = $this->fleetRepository->findById($fleetId) ?? throw new FleetNotFoundException($fleetId->toNative());

        $plateNumber = $localizeVehicleQuery->plateNumber();

        $vehicle = $this->vehicleRepository->findByPlateNumber($plateNumber)
            ?? throw new VehicleNotFoundException($plateNumber->toNative());

        return $fleet->localizeVehicle($vehicle);
    }
}
