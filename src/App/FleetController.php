<?php

namespace Fulll\App;

use Dotenv\Dotenv;
use Exception;
use Fulll\App\Fleet\Exception\FleetNotFoundException;
use Fulll\App\Fleet\Exception\VehicleNotFoundException;
use Fulll\App\Fleet\FleetService;
use Fulll\Bootloader\Bootstrap;
use Fulll\Domain\Fleet\Exception\DuplicateVehicleException;
use Fulll\Domain\Fleet\Exception\VehicleDoesntBelongToFleetException;
use Fulll\Domain\Fleet\Exception\VehicleNotParkedException;
use Fulll\Domain\Shared\Exception\IncompatibleForEqualityCheckException;
use Fulll\Domain\Shared\Exception\InvalidIdException;

class FleetController
{
    private readonly FleetService $fleetService;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        require_once __DIR__ . '/../../vendor/autoload.php';

        $dotenv = Dotenv::createImmutable(__DIR__ . '/../../', '.env');
        $dotenv->load();

        $app = Bootstrap::run();
        $container = $app->loadServiceContainer();

        $this->fleetService = $container->get(FleetService::class);
    }

    /**
     * @throws InvalidIdException
     */
    public function createFleet(string $userId): string
    {
        return $this->fleetService->createFleet($userId);
    }

    /**
     * @throws InvalidIdException
     * @throws FleetNotFoundException
     * @throws IncompatibleForEqualityCheckException
     * @throws DuplicateVehicleException
     */
    public function registerVehicleIntoFleet(string $fleetId, string $plateNumber): void
    {
        $this->fleetService->registerVehicleIntoFleet($fleetId, $plateNumber);
    }

    /**
     * @throws FleetNotFoundException
     * @throws InvalidIdException
     * @throws IncompatibleForEqualityCheckException
     * @throws VehicleDoesntBelongToFleetException
     * @throws VehicleNotFoundException
     */
    public function parkVehicleToLocation(
        string $fleetId,
        string $plateNumber,
        float $latitude,
        float $longitude,
        ?int $altitude = null
    ): void {
        $this->fleetService->parkVehicleToLocation($fleetId, $plateNumber, $latitude, $longitude, $altitude);
    }

    /**
     * @param string $fleetId
     * @param string $plateNumber
     * @return array{
     *     latitude: float,
     *     longitude: float,
     *     altitude: int
     * }
     *
     * @throws FleetNotFoundException
     * @throws VehicleNotFoundException
     * @throws VehicleDoesntBelongToFleetException
     * @throws VehicleNotParkedException
     * @throws InvalidIdException
     */
    public function localizeVehicle(string $fleetId, string $plateNumber): array
    {
        return $this->fleetService->localizeVehicle($fleetId, $plateNumber);
    }
}
