<?php

namespace Fulll\App;

use Fulll\App\Contracts\Message;
use Fulll\App\Contracts\MessageBusInterface;
use Fulll\App\Contracts\MessageHandler;
use Fulll\App\Exception\MissingHandlerException;

class MessageBus implements MessageBusInterface
{
    /** @var MessageHandler[]  */
    private array $handlers = [];

    public function registerHandler(string $messageName, MessageHandler $handler): self
    {
        $this->handlers[$messageName] = $handler;

        return $this;
    }

    /**
     * @throws MissingHandlerException
     */
    public function send(Message $command): mixed
    {
        $handler = $this->handlers[$command::class] ?? throw new MissingHandlerException($command::class);

        return $handler->handle($command);
    }
}
