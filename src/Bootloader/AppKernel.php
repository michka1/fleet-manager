<?php

namespace Fulll\Bootloader;

use Exception;
use Fulll\App\Contracts\MessageBusInterface;
use Fulll\App\Contracts\MessageHandler;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class AppKernel
{
    /**
     * @var string[]
     */
    private array $commandHandlers;
    private ContainerBuilder $container;
    /**
     * @var string[]
     */
    private array $queryHandlers;

    /**
     * @throws Exception
     */
    public function boot(): void
    {
        $configPath = __DIR__ . '/../../config';
        $this->commandHandlers = require $configPath . '/commands.php';
        $this->queryHandlers = require $configPath . '/queries.php';

        $this->loadServiceContainer();
        $this->configureMessageBus();
    }

    public function getContainer(): ContainerBuilder
    {
        return $this->container;
    }

    /**
     * @throws Exception
     */
    private function loadServiceContainer(): void
    {
        $this->container = new ContainerBuilder();

        $isTestingEnv = $_ENV['APP_ENV'] === 'test';

        $dbName = $isTestingEnv ? $_ENV['DB_TEST_NAME'] : $_ENV['DB_NAME'];

        $this->container->setParameter('database.host', $_ENV['DB_HOST']);
        $this->container->setParameter('database.dbname', $dbName);
        $this->container->setParameter('database.user', $_ENV['DB_USER']);
        $this->container->setParameter('database.password', $_ENV['DB_PASSWORD']);

        $loader = new YamlFileLoader($this->container, new FileLocator(__DIR__ . '/../../config'));
        $loader->load($isTestingEnv ? 'services_test.yaml' : 'services.yaml');

        $this->container->compile();
    }

    /**
     * @throws Exception
     */
    private function configureMessageBus(): void
    {
        /** @var MessageBusInterface $commandBus */
        $commandBus = $this->container->get('MessageBus');

        /**
         * @var string $command
         * @var string $handlerId
         */
        foreach ($this->commandHandlers as $command => $handlerId) {
            /** @var MessageHandler $handler */
            $handler = $this->container->get($handlerId);

            $commandBus->registerHandler($command, $handler);
        }

        /**
         * @var string $query
         * @var string $handlerId
         */
        foreach ($this->queryHandlers as $query => $handlerId) {
            /** @var MessageHandler $handler */
            $handler = $this->container->get($handlerId);

            $commandBus->registerHandler($query, $handler);
        }
    }
}
