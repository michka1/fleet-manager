<?php

namespace Fulll\Bootloader;

use Exception;
use Symfony\Component\DependencyInjection\ContainerBuilder;

final class Bootstrap
{
    private static ContainerBuilder $container;

    /**
     * @throws Exception
     */
    private function __construct()
    {
        $kernel = new AppKernel();
        $kernel->boot();

        self::$container = $kernel->getContainer();
    }

    /**
     * @throws Exception
     */
    public static function run(): static
    {
        return new static();
    }

    public function loadServiceContainer(): ContainerBuilder
    {
        return self::$container;
    }
}
