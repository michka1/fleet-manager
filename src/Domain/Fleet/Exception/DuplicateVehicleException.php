<?php

namespace Fulll\Domain\Fleet\Exception;

use Exception;

class DuplicateVehicleException extends Exception
{
    public function __construct(string $plateNumber)
    {

        $message = sprintf('Vehicle with plate number: "%s" already belongs to the fleet', $plateNumber);

        parent::__construct($message);
    }
}
