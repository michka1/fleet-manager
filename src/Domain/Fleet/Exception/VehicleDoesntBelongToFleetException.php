<?php

namespace Fulll\Domain\Fleet\Exception;

use Exception;

class VehicleDoesntBelongToFleetException extends Exception
{
    public function __construct(string $plateNumber)
    {

        $message = sprintf('Vehicle with plate number: "%s" doesn\'t belong to your fleet', $plateNumber);

        parent::__construct($message);
    }
}
