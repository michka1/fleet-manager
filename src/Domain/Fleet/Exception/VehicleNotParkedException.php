<?php

namespace Fulll\Domain\Fleet\Exception;

use Exception;

class VehicleNotParkedException extends Exception
{
    public function __construct(string $plateNumber)
    {
        $message = sprintf('Vehicle %s is not parked anywhere', $plateNumber);

        parent::__construct($message);
    }
}
