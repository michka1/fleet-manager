<?php

namespace Fulll\Domain\Fleet;

use Fulll\Domain\Fleet\Characteristics\FleetId;
use Fulll\Domain\Fleet\Characteristics\UserId;
use Fulll\Domain\Fleet\Exception\DuplicateVehicleException;
use Fulll\Domain\Fleet\Exception\VehicleDoesntBelongToFleetException;
use Fulll\Domain\Fleet\Exception\VehicleNotParkedException;
use Fulll\Domain\Shared\Exception\IncompatibleForEqualityCheckException;
use Fulll\Domain\Vehicle\Characteristics\Location;
use Fulll\Domain\Vehicle\Characteristics\VehicleId;
use Fulll\Domain\Vehicle\Exception\InvalidCoordinatesException;
use Fulll\Domain\Vehicle\Vehicle;

class Fleet
{
    /**
     * @param VehicleId[] $vehicleIds
     */
    public function __construct(
        private readonly FleetId $id,
        private readonly UserId $userId,
        private readonly ?string $name = null,
        private array $vehicleIds = []
    ) {
    }

    /**
     * @throws DuplicateVehicleException
     * @throws IncompatibleForEqualityCheckException
     */
    public function registerVehicle(Vehicle $vehicle): void
    {
        if ($this->contains($vehicle)) {
            throw new DuplicateVehicleException($vehicle->plateNumber()->toNative());
        }

        $this->vehicleIds[] = $vehicle->id();
    }

    public function id(): FleetId
    {
        return $this->id;
    }

    public function userId(): UserId
    {
        return $this->userId;
    }

    public function name(): ?string
    {
        return $this->name;
    }

    /**
     * @return VehicleId[]
     */
    public function vehicleIds(): array
    {
        return $this->vehicleIds;
    }

    /**
     * @throws IncompatibleForEqualityCheckException
     */
    public function contains(Vehicle $vehicle): bool
    {
        foreach ($this->vehicleIds as $inFleetVehicleId) {
            if ($vehicle->id()->equals($inFleetVehicleId)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @throws VehicleDoesntBelongToFleetException
     * @throws IncompatibleForEqualityCheckException
     */
    public function parkVehicle(Vehicle $vehicle, Location $newLocation): void
    {
        if (!$this->contains($vehicle)) {
            throw new VehicleDoesntBelongToFleetException($vehicle->plateNumber()->toNative());
        }

        // todo: inquire with the business team regarding their criteria for considering two positions as 'different'
        $vehicleLocation = $vehicle->location();
        if (!is_null($vehicleLocation) && $vehicleLocation->equals($newLocation)) {
            throw new InvalidCoordinatesException("this vehicle was already parked to this location.");
        }

        $vehicle->park($newLocation);
    }

    /**
     * @throws VehicleNotParkedException
     * @throws VehicleDoesntBelongToFleetException
     * @throws IncompatibleForEqualityCheckException
     */
    public function localizeVehicle(Vehicle $vehicle): Location
    {
        if (!$this->contains($vehicle)) {
            throw new VehicleDoesntBelongToFleetException($vehicle->plateNumber()->toNative());
        }

        if ($vehicle->location() === null) {
            throw new VehicleNotParkedException($vehicle->plateNumber()->toNative());
        }

        return $vehicle->location();
    }
}
