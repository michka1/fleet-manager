<?php

namespace Fulll\Domain\Fleet\Repository;

use Fulll\Domain\Fleet\Characteristics\FleetId;
use Fulll\Domain\Fleet\Fleet;

interface FleetRepository
{
    public function findById(FleetId $id): ?Fleet;

    public function create(Fleet $fleet): void;

    public function update(Fleet $fleet): void;
}
