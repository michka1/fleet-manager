<?php

namespace Fulll\Domain\Shared;

use Fulll\Domain\Shared\Exception\IncompatibleForEqualityCheckException;
use Fulll\Domain\Shared\Exception\InvalidIdException;
use Ramsey\Uuid\Uuid;

abstract class BaseId implements ValueObject
{
    /**
     * @throws InvalidIdException
     */
    final private function __construct(private readonly string $id)
    {
        if (!Uuid::isValid($id)) {
            throw new InvalidIdException($id);
        }
    }

    /**
     * @throws InvalidIdException
     */
    final public static function generate(): static
    {
        $id = Uuid::uuid4()->toString();

        return new static($id);
    }

    /**
     * @throws InvalidIdException
     */
    public static function fromNative(string $value): static
    {
        return new static($value);
    }

    public function toNative(): string
    {
        return $this->id;
    }

    /**
     * @throws IncompatibleForEqualityCheckException
     */
    public function equals(mixed $comparisonElement): bool
    {
        if (($comparisonElement instanceof static) === false) {
            throw new IncompatibleForEqualityCheckException();
        }

        return $comparisonElement->toNative() === $this->toNative();
    }
}
