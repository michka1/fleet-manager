<?php

namespace Fulll\Domain\Shared\Exception;

use Exception;

class IncompatibleForEqualityCheckException extends Exception
{
    public function __construct()
    {
        parent::__construct('Type of operands, impossible to test equality');
    }
}
