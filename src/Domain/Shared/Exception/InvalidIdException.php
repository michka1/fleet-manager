<?php

namespace Fulll\Domain\Shared\Exception;

use Exception;

class InvalidIdException extends Exception
{
    public function __construct(string $id)
    {
        parent::__construct(
            sprintf('The provided ID: "%s" has an invalid format', $id)
        );
    }
}
