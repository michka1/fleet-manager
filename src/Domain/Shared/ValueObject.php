<?php

namespace Fulll\Domain\Shared;

interface ValueObject
{
    public function equals(mixed $comparisonElement): bool;
}
