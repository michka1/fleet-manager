<?php

namespace Fulll\Domain\Vehicle\Characteristics;

use Fulll\Domain\Shared\Exception\IncompatibleForEqualityCheckException;
use Fulll\Domain\Shared\ValueObject;
use Fulll\Domain\Vehicle\Exception\InvalidCoordinatesException;

final class Location implements ValueObject
{
    private const PRECISION = 6;

    private const MAX_LATITUDE = 90;
    private const MAX_LONGITUDE = 180;
    /**
     * highest point on Earth, Mount Everest :)
     */
    private const MAX_ALTITUDE = 8849;

    public function __construct(
        private float $latitude,
        private float $longitude,
        private readonly ?int $altitude = null
    ) {
        $this->validateCoordinates();

        $this->latitude = round($this->latitude, self::PRECISION);
        $this->longitude = round($this->longitude, self::PRECISION);
    }

    /**
     * @throws InvalidCoordinatesException
     */
    private function validateCoordinates(): void
    {
        $this->validateLatitude();
        $this->validateLongitude();
        $this->validateAltitude();
    }

    /**
     * @throws InvalidCoordinatesException
     */
    private function validateLatitude(): void
    {
        if ($this->latitude < 0) {
            throw new InvalidCoordinatesException('Latitude cannot be negative.');
        }
        if ($this->latitude > self::MAX_LATITUDE) {
            throw new InvalidCoordinatesException('Latitude cannot exceed ' . self::MAX_LATITUDE . '.');
        }
    }

    /**
     * @throws InvalidCoordinatesException
     */
    private function validateLongitude(): void
    {
        if ($this->longitude < 0) {
            throw new InvalidCoordinatesException('Longitude cannot be negative.');
        }
        if ($this->longitude > self::MAX_LONGITUDE) {
            throw new InvalidCoordinatesException('Longitude cannot exceed ' . self::MAX_LONGITUDE . '.');
        }
    }

    /**
     * @throws InvalidCoordinatesException
     */
    private function validateAltitude(): void
    {
        if ($this->altitude < 0) {
            throw new InvalidCoordinatesException('Altitude cannot be negative.');
        }
        if ($this->altitude > self::MAX_ALTITUDE) {
            throw new InvalidCoordinatesException('Altitude cannot exceed ' . self::MAX_ALTITUDE . '.');
        }
    }

    public function latitude(): float
    {
        return $this->latitude;
    }

    public function longitude(): float
    {
        return $this->longitude;
    }

    public function altitude(): ?int
    {
        return $this->altitude;
    }

    /**
     * @throws InvalidCoordinatesException
     */
    public static function fromNative(float $latitude, float $longitude, ?int $altitude = null): static
    {
        return new static($latitude, $longitude, $altitude);
    }

    /**
     * @throws IncompatibleForEqualityCheckException
     */
    public function equals(mixed $comparisonElement): bool
    {
        if (($comparisonElement instanceof static) === false) {
            throw new IncompatibleForEqualityCheckException();
        }

        return
            $this->latitude === $comparisonElement->latitude
            && $this->longitude === $comparisonElement->longitude
            && $this->altitude === $comparisonElement->altitude
        ;
    }
}
