<?php

namespace Fulll\Domain\Vehicle\Characteristics;

use Fulll\Domain\Shared\Exception\IncompatibleForEqualityCheckException;
use Fulll\Domain\Shared\ValueObject;
use Fulll\Domain\Vehicle\Exception\InvalidPlateNumberException;

final class PlateNumber implements ValueObject
{
    private const PLATE_NUMBER_VALIDATION_PATTERN =
        "/^[A-HJ-NP-TV-Z]{2}\s*-\s*(?:00[1-9]|[0-9]{3})\s*-\s*[A-HJ-NP-TV-Z]{2}$/"
    ;

    /**
     * @throws InvalidPlateNumberException
     */
    private function __construct(private readonly string $plateNumber)
    {
        if (!preg_match(self::PLATE_NUMBER_VALIDATION_PATTERN, $plateNumber)) {
            throw new InvalidPlateNumberException($this->plateNumber);
        }
    }

    public function toNative(): string
    {
        return $this->plateNumber;
    }

    /**
     * @throws InvalidPlateNumberException
     */
    public static function fromNative(string $value): PlateNumber
    {
        return new static($value);
    }

    /**
     * @throws IncompatibleForEqualityCheckException
     */
    public function equals(mixed $comparisonElement): bool
    {
        if (($comparisonElement instanceof PlateNumber) === false) {
            throw new IncompatibleForEqualityCheckException();
        }

        return $comparisonElement->toNative() === $this->toNative();
    }
}
