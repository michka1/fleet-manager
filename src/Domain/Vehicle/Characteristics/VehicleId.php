<?php

namespace Fulll\Domain\Vehicle\Characteristics;

use Fulll\Domain\Shared\BaseId;

final class VehicleId extends BaseId
{
}
