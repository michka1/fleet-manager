<?php

namespace Fulll\Domain\Vehicle\Exception;

use InvalidArgumentException;

class InvalidCoordinatesException extends InvalidArgumentException
{
    public function __construct(string $message)
    {
        parent::__construct("Invalid coordinates provided: " . $message);
    }
}
