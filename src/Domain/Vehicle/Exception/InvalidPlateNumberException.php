<?php

namespace Fulll\Domain\Vehicle\Exception;

use InvalidArgumentException;

class InvalidPlateNumberException extends InvalidArgumentException
{
    public function __construct(string $plateNumber)
    {
        parent::__construct(sprintf('The given plate number seems invalid: "%s"', $plateNumber));
    }
}
