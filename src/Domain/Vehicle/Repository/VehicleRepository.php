<?php

namespace Fulll\Domain\Vehicle\Repository;

use Fulll\Domain\Vehicle\Characteristics\PlateNumber;
use Fulll\Domain\Vehicle\Vehicle;

interface VehicleRepository
{
    public function findByPlateNumber(PlateNumber $plateNumber): ?Vehicle;

    public function create(Vehicle $vehicle): void;

    public function update(Vehicle $vehicle): void;
}
