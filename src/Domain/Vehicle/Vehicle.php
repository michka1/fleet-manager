<?php

namespace Fulll\Domain\Vehicle;

use Fulll\Domain\Vehicle\Characteristics\Location;
use Fulll\Domain\Vehicle\Characteristics\PlateNumber;
use Fulll\Domain\Vehicle\Characteristics\VehicleId;

class Vehicle
{
    public function __construct(
        private readonly VehicleId $id,
        private readonly PlateNumber $plateNumber,
        private ?Location $location = null
    ) {
    }

    public function id(): VehicleId
    {
        return $this->id;
    }

    public function plateNumber(): PlateNumber
    {
        return $this->plateNumber;
    }

    public function location(): ?Location
    {
        return $this->location;
    }

    public function park(Location $location): self
    {
        $this->location = $location;

        return $this;
    }
}
