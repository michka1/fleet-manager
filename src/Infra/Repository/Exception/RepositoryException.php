<?php

namespace Fulll\Infra\Repository\Exception;

use Exception;

class RepositoryException extends Exception
{
    public function __construct(string $message = null)
    {
        parent::__construct($message);
    }
}
