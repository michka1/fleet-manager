<?php

namespace Fulll\Infra\Repository\Exception;

class RepositoryTypeDetectionException extends RepositoryException
{
}
