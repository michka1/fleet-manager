<?php

namespace Fulll\Infra\Repository\Factory;

use Fulll\Infra\DbConnection;
use Fulll\Infra\Repository\Exception\RepositoryTypeDetectionException;
use Throwable;

class RepositoryFactory
{
    /**
     * @throws RepositoryTypeDetectionException
     */
    public static function create(string $type, DbConnection $dbConnection): mixed
    {
        $type = RepositoryType::from($type);

        $className = ucfirst($_ENV['REPOSITORY_TYPE']) . $type->value . 'Repository';
        $fullClassName = 'Fulll\Infra\Repository\\' . $className;

        try {
            return new $fullClassName($dbConnection);
        } catch (Throwable $e) {
            throw new RepositoryTypeDetectionException(
                'Error while defining repository type:'
                . $e->getMessage()
            );
        }
    }
}
