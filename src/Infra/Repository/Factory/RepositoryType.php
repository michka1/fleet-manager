<?php

namespace Fulll\Infra\Repository\Factory;

enum RepositoryType: string
{
    case Fleet = 'Fleet';
    case Vehicle = 'Vehicle';
}
