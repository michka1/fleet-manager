<?php

namespace Fulll\Infra\Repository;

use Fulll\Domain\Fleet\Characteristics\FleetId;
use Fulll\Domain\Fleet\Fleet;
use Fulll\Domain\Fleet\Repository\FleetRepository;

final class InMemoryFleetRepository implements FleetRepository
{
    /**
     * @var Fleet[]
     */
    private array $fleets = [];

    public function findById(FleetId $id): ?Fleet
    {
        return $this->fleets[$id->toNative()] ?? null;
    }

    public function create(Fleet $fleet): void
    {
        $this->fleets[$fleet->id()->toNative()] = $fleet;
    }

    public function update(Fleet $fleet): void
    {
        $this->create($fleet);
    }
}
