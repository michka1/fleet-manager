<?php

namespace Fulll\Infra\Repository;

use Fulll\Domain\Shared\Exception\IncompatibleForEqualityCheckException;
use Fulll\Domain\Vehicle\Characteristics\PlateNumber;
use Fulll\Domain\Vehicle\Repository\VehicleRepository;
use Fulll\Domain\Vehicle\Vehicle;

final class InMemoryVehicleRepository implements VehicleRepository
{
    /**
     * @var Vehicle[]
     */
    private array $vehicles = [];

    /**
     * @throws IncompatibleForEqualityCheckException
     */
    public function findByPlateNumber(PlateNumber $plateNumber): ?Vehicle
    {
        foreach ($this->vehicles as $vehicle) {
            if ($vehicle->plateNumber()->equals($plateNumber)) {
                return $vehicle;
            }
        }

        return null;
    }

    public function create(Vehicle $vehicle): void
    {
        $this->vehicles[$vehicle->id()->toNative()] = $vehicle;
    }

    public function update(Vehicle $vehicle): void
    {
        $this->create($vehicle);
    }
}
