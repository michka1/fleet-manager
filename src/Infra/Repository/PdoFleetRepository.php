<?php

namespace Fulll\Infra\Repository;

use Fulll\Domain\Fleet\Characteristics\FleetId;
use Fulll\Domain\Fleet\Characteristics\UserId;
use Fulll\Domain\Fleet\Fleet;
use Fulll\Domain\Fleet\Repository\FleetRepository;
use Fulll\Domain\Shared\Exception\InvalidIdException;
use Fulll\Domain\Vehicle\Characteristics\VehicleId;
use Fulll\Infra\DbConnection;
use PDO;

class PdoFleetRepository implements FleetRepository
{
    private readonly PDO $dbh;

    public function __construct(private readonly DbConnection $db)
    {
        $this->dbh = $this->db->getConnection();
    }

    /**
     * @throws InvalidIdException
     */
    public function findById(FleetId $id): ?Fleet
    {
        $query = "SELECT * FROM fleet WHERE id = :id";

        $stmt = $this->dbh->prepare($query);
        $stmt->bindValue(':id', $id->toNative());
        $stmt->execute();

        $fleetData = $stmt->fetch(PDO::FETCH_OBJ);

        if (!$fleetData) {
            return null;
        }

        return new Fleet(
            FleetId::fromNative($fleetData->id),
            UserId::fromNative($fleetData->user_id),
            $fleetData->name,
            array_map(
                fn($nativeVehicleId) => VehicleId::fromNative($nativeVehicleId),
                $this->getCurrentVehicleIds($id)
            )
        );
    }

    public function create(Fleet $fleet): void
    {
        $stmt = $this->dbh->prepare("INSERT INTO fleet (id, user_id, name) VALUES (:id, :user_id, :name)");

        $stmt->execute([
            'id' => $fleet->id()->toNative(),
            'user_id' => $fleet->userId()->toNative(),
            'name' => $fleet->name(),
        ]);
    }

    public function update(Fleet $fleet): void
    {
        $currentVehicleIds = $this->getCurrentVehicleIds($fleet->id());

        $vehicleIdsToAdd = array_diff(
            array_map(fn($vehicleId)=>$vehicleId->toNative(), $fleet->vehicleIds()),
            $currentVehicleIds
        );

        $stmt = $this->dbh->prepare('INSERT INTO fleet_vehicle SET fleet_id=:fleet_id, vehicle_id=:vehicle_id');

        foreach ($vehicleIdsToAdd as $vehicleIdToAdd) {
            $stmt->execute([
                'fleet_id' => $fleet->id()->toNative(),
                'vehicle_id' => $vehicleIdToAdd
            ]);
        }
    }

    /**
     * @return string[]
     */
    private function getCurrentVehicleIds(FleetId $fleetId): array
    {
        $stmt = $this->dbh->prepare("SELECT vehicle_id FROM fleet_vehicle WHERE fleet_id=:id");

        $stmt->execute([
            'id' => $fleetId->toNative(),
        ]);

        return array_map(
            fn($result)=>$result['vehicle_id'],
            $stmt->fetchAll(PDO::FETCH_ASSOC)
        );
    }
}
