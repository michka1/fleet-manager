<?php

namespace Fulll\Infra\Repository;

use Fulll\Domain\Vehicle\Characteristics\Location;
use Fulll\Domain\Vehicle\Characteristics\PlateNumber;
use Fulll\Domain\Vehicle\Characteristics\VehicleId;
use Fulll\Domain\Vehicle\Repository\VehicleRepository;
use Fulll\Domain\Vehicle\Vehicle;
use Fulll\Infra\DbConnection;
use PDO;

class PdoVehicleRepository implements VehicleRepository
{
    private readonly PDO $dbh;

    public function __construct(private readonly DbConnection $db)
    {
        $this->dbh = $this->db->getConnection();
    }

    public function findByPlateNumber(PlateNumber $plateNumber): ?Vehicle
    {
        $stmt = $this->dbh->prepare('SELECT * FROM vehicle WHERE plate_number=:plate_number');

        $stmt->execute(['plate_number' => $plateNumber->toNative(),]);

        if (!$data = $stmt->fetch(PDO::FETCH_OBJ)) {
            return null;
        }

        $location = !is_null($data->latitude ?? $data->longitude)
            ? Location::fromNative($data->latitude, $data->longitude, $data->altitude)
            : null
        ;

        return new Vehicle(
            VehicleId::fromNative($data->id),
            PlateNumber::fromNative($data->plate_number),
            $location
        );
    }

    public function create(Vehicle $vehicle): void
    {
        $query = '
            INSERT INTO vehicle (id, plate_number, latitude, longitude, altitude) 
            VALUES (:id, :plate_number, :latitude, :longitude, :altitude)
        ';

        $stmt = $this->dbh->prepare($query);

        $stmt->execute([
            'id' => $vehicle->id()->toNative(),
            'plate_number' => $vehicle->plateNumber()->toNative(),
            'latitude' => $vehicle->location()?->latitude(),
            'longitude' => $vehicle->location()?->longitude(),
            'altitude' => $vehicle->location()?->altitude(),
        ]);
    }

    public function update(Vehicle $vehicle): void
    {
        $query = '
            UPDATE vehicle SET latitude=:latitude, longitude=:longitude, altitude=:altitude WHERE id=:id
        ';

        $stmt = $this->dbh->prepare($query);

        $stmt->execute([
            'id' => $vehicle->id()->toNative(),
            'latitude' => $vehicle->location()?->latitude(),
            'longitude' => $vehicle->location()?->longitude(),
            'altitude' => $vehicle->location()?->altitude(),
        ]);
    }
}
