<?php

namespace Fulll\Ui\Command;

use Exception;
use Fulll\App\FleetController;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateFleet extends Command
{
    protected static $defaultName = 'create';

    public function __construct(private readonly FleetController $fleetController)
    {
        parent::__construct();
    }

    public function configure(): void
    {
        $this
            ->setDescription('Create a new fleet')
            ->addArgument('userId', InputArgument::REQUIRED, 'must be a valid UUID')
            ->addArgument('name', InputArgument::OPTIONAL, 'a string representing the name of your fleet')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $userId = $input->getArgument('userId');

        try {
            $fleetId = $this->fleetController->createFleet($userId);
        } catch (Exception $e) {
            $output->writeln($e->getMessage());

            return Command::FAILURE;
        }

        $output->writeln($fleetId);

        return Command::SUCCESS;
    }
}
