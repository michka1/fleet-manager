<?php

namespace Fulll\Ui\Command;

use Exception;
use Fulll\App\FleetController;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LocalizeVehicle extends Command
{
    protected static $defaultName = 'localize-vehicle';

    public function __construct(private readonly FleetController $fleetController)
    {
        parent::__construct();
    }

    public function configure(): void
    {
        $this
            ->setDescription('Displays where a vehicle is parked.')
            ->addArgument('fleetId', InputArgument::REQUIRED, 'The ID of the fleet')
            ->addArgument('vehiclePlateNumber', InputArgument::REQUIRED, 'The plate number of the vehicle')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $parkingCoordinates = $this->fleetController->localizeVehicle(
                $input->getArgument('fleetId'),
                $input->getArgument('vehiclePlateNumber'),
            );
        } catch (Exception $e) {
            $output->writeln($e->getMessage());

            return Command::FAILURE;
        }

        $output->writeln('Your vehicle is currently parked at this location');
        $output->writeln('     - latitude: ' . $parkingCoordinates['latitude']);
        $output->writeln('     - longitude: ' . $parkingCoordinates['longitude']);
        $output->writeln('     - altitude: ' . ($parkingCoordinates['altitude'] ?? '0'));

        return Command::SUCCESS;
    }
}
