<?php

namespace Fulll\Ui\Command;

use Exception;
use Fulll\App\FleetController;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ParkVehicleToLocation extends Command
{
    protected static $defaultName = 'park-vehicle';

    public function __construct(private readonly FleetController $fleetController)
    {
        parent::__construct();
    }

    public function configure(): void
    {
        $this
            ->setDescription('Park a vehicle.')
            ->addArgument('fleetId', InputArgument::REQUIRED, 'The ID of the fleet')
            ->addArgument('vehiclePlateNumber', InputArgument::REQUIRED, 'The plate number of the vehicle')
            ->addArgument('lat', InputArgument::REQUIRED, 'Latitude')
            ->addArgument('lng', InputArgument::REQUIRED, 'Longitude')
            ->addArgument('alt', InputArgument::OPTIONAL, 'Altitude')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $this->fleetController->parkVehicleToLocation(
                $input->getArgument('fleetId'),
                $input->getArgument('vehiclePlateNumber'),
                $input->getArgument('lat'),
                $input->getArgument('lng'),
                $input->getArgument('alt'),
            );
        } catch (Exception $e) {
            $output->writeln($e->getMessage());

            return Command::FAILURE;
        }

        $output->writeln('Your vehicle was successfully parked!');

        return Command::SUCCESS;
    }
}
