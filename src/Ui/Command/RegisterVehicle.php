<?php

namespace Fulll\Ui\Command;

use Exception;
use Fulll\App\FleetController;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RegisterVehicle extends Command
{
    protected static $defaultName = 'register-vehicle';

    public function __construct(private readonly FleetController $fleetController)
    {
        parent::__construct();
    }

    public function configure(): void
    {
        $this
            ->setDescription('Register a vehicle into my fleet')
            ->addArgument('fleetId', InputArgument::REQUIRED, 'must be a valid UUID')
            ->addArgument('vehiclePlateNumber', InputArgument::REQUIRED, 'a valid plate number')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $this->fleetController->registerVehicleIntoFleet(
                $input->getArgument('fleetId'),
                $input->getArgument('vehiclePlateNumber')
            );
        } catch (Exception $e) {
            $output->writeln($e->getMessage());

            return Command::FAILURE;
        }

        $output->writeln('Your vehicle was successfully registered!');

        return Command::SUCCESS;
    }
}
