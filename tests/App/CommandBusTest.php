<?php

namespace App;

use Fulll\App\Contracts\Message;
use Fulll\App\Contracts\MessageHandler;
use Fulll\App\Exception\MissingHandlerException;
use Fulll\App\MessageBus;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

class CommandBusTest extends TestCase
{

    private MessageBus $commandBus;

    protected function setUp(): void
    {
        $this->commandBus = new MessageBus();
    }

    public function testRegisterHandlerAndSuccessfulSend()
    {
        $commandMock = $this->createMock(Message::class);

        $handlerMock = $this->getMockBuilder(MessageHandler::class)
            ->addMethods(['handle'])
            ->getMock()
        ;

        $handlerMock
            ->expects($this->once())
            ->method('handle')->with($commandMock)->willReturn('hello')
        ;

        $this->commandBus->registerHandler($commandMock::class, $handlerMock);

        $this->assertEquals('hello', $this->commandBus->send($commandMock));

    }

    public function testHandlerNotFound()
    {
        $commandMock = $this->createMock(Message::class);

        $this->expectException(MissingHandlerException::class);
        $this->commandBus->send($commandMock);
    }

    public function testRegistrationHandler()
    {
        $handlerMock = $this->createMock(MessageHandler::class);
        $commandName = 'TestCommand';

        $this->commandBus->registerHandler($commandName, $handlerMock);

        $reflection = new ReflectionClass(MessageBus::class);
        $handlers = $reflection->getProperty('handlers');
        $handlersValue = $handlers->getValue($this->commandBus);

        $this->assertArrayHasKey($commandName, $handlersValue);
        $this->assertSame($handlerMock, $handlersValue[$commandName]);
    }
}
