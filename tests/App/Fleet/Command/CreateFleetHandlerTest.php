<?php

namespace App\Fleet\Command;

use Fulll\App\Fleet\Command\CreateFleet;
use Fulll\App\Fleet\Command\CreateFleetHandler;
use Fulll\Domain\Fleet\Characteristics\FleetId;
use Fulll\Domain\Fleet\Fleet;
use Fulll\Domain\Fleet\Repository\FleetRepository;
use PHPUnit\Framework\TestCase;

class CreateFleetHandlerTest extends TestCase
{
    public function testSuccessfulHandle()
    {
        $expectedFleetId= FleetId::generate();
        $fleetName = 'My amazing fleet';

        $fleetRepository = $this->createMock(FleetRepository::class);

        $createFleet = $this->createMock(CreateFleet::class);


        $createFleet
            ->expects($this->once())
            ->method('id')->willReturn($expectedFleetId)
        ;

        $createFleet
            ->expects($this->once())
            ->method('name')->willReturn($fleetName)
        ;

        $fleetRepository
            ->expects($this->once())
            ->method('create')
            ->with($this->callback(fn($fleet) =>
                $fleet instanceof Fleet
                && $fleet->id() === $expectedFleetId
                && $fleet->name() == $fleetName
            ))
        ;

        $createFleetHandler = new CreateFleetHandler($fleetRepository);
        $fleet = $createFleetHandler->handle($createFleet);

        $this->assertEquals($expectedFleetId->toNative(), $fleet->id()->toNative());
        $this->assertEquals($fleetName, $fleet->name());
    }
}
