<?php
namespace App\Fleet\Command;

use Fulll\App\Fleet\Command\ParkVehicle;
use Fulll\App\Fleet\Command\ParkVehicleHandler;
use Fulll\App\Fleet\Exception\FleetNotFoundException;
use Fulll\App\Fleet\Exception\VehicleNotFoundException;
use Fulll\Domain\Fleet\Characteristics\FleetId;
use Fulll\Domain\Fleet\Fleet;
use Fulll\Domain\Fleet\Repository\FleetRepository;
use Fulll\Domain\Vehicle\Characteristics\Location;
use Fulll\Domain\Vehicle\Characteristics\PlateNumber;
use Fulll\Domain\Vehicle\Repository\VehicleRepository;
use Fulll\Domain\Vehicle\Vehicle;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class ParkVehicleHandlerTest extends TestCase
{
    private MockObject $fleetRepository;
    private MockObject $vehicleRepository;
    private ParkVehicleHandler $parkVehicleHandler;
    private ParkVehicle $command;

    protected function setUp(): void
    {
        $this->fleetRepository = $this->createMock(FleetRepository::class);
        $this->vehicleRepository = $this->createMock(VehicleRepository::class);
        $this->parkVehicleHandler = new ParkVehicleHandler($this->fleetRepository, $this->vehicleRepository);

        $this->command = new ParkVehicle(
            FleetId::generate(),
            PlateNumber::fromNative('AA-123-BA'),
            Location::fromNative(0,0)
        );
    }

    public function testHandleThrowsFleetNotFoundException()
    {
        $this->fleetRepository->method('findById')->willReturn(null);

        $this->expectException(FleetNotFoundException::class);
        $this->parkVehicleHandler->handle($this->command);
    }

    public function testHandleThrowsVehicleNotFoundException()
    {
        $this->fleetRepository->method('findById')->willReturn($this->createMock(Fleet::class));
        $this->vehicleRepository->method('findByPlateNumber')->willReturn(null);

        $this->expectException(VehicleNotFoundException::class);
        $this->parkVehicleHandler->handle($this->command);
    }

    public function testHandleSuccess()
    {
        $fleet = $this->createMock(Fleet::class);
        $vehicle = $this->createMock(Vehicle::class);

        $this->fleetRepository->method('findById')->willReturn($fleet);
        $this->vehicleRepository->method('findByPlateNumber')->willReturn($vehicle);

        $fleet
            ->expects($this->once())
            ->method('parkVehicle')->with($vehicle, $this->command->location())
        ;

        $this->vehicleRepository
            ->expects($this->once())
            ->method('update')->with($vehicle)
        ;

        $this->parkVehicleHandler->handle($this->command);
    }
}
