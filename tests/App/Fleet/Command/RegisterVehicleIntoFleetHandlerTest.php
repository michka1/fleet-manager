<?php

namespace App\Fleet\Command;

use Fulll\App\Fleet\Command\RegisterVehicleIntoFleet;
use Fulll\App\Fleet\Command\RegisterVehicleIntoFleetHandler;
use Fulll\App\Fleet\Exception\FleetNotFoundException;
use Fulll\Domain\Fleet\Fleet;
use Fulll\Domain\Fleet\Repository\FleetRepository;
use Fulll\Domain\Vehicle\Characteristics\PlateNumber;
use Fulll\Domain\Vehicle\Characteristics\VehicleId;
use Fulll\Domain\Vehicle\Repository\VehicleRepository;
use Fulll\Domain\Vehicle\Vehicle;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class RegisterVehicleIntoFleetHandlerTest extends TestCase
{
    private MockObject $fleetRepository;
    private MockObject $vehicleRepository;
    private RegisterVehicleIntoFleetHandler $registerVehicleIntoFleetHandler;
    private Fleet $fleet;
    private Vehicle $vehicle;
    private RegisterVehicleIntoFleet $command;

    protected function setUp(): void
    {
        $this->fleetRepository = $this->createMock(FleetRepository::class);
        $this->vehicleRepository = $this->createMock(VehicleRepository::class);
        $this->fleet = $this->createMock(Fleet::class);

        $this->registerVehicleIntoFleetHandler = new RegisterVehicleIntoFleetHandler(
            $this->fleetRepository,
            $this->vehicleRepository
        );

        $this->vehicle = new Vehicle(VehicleId::generate(), PlateNumber::fromNative('AB-123-CD'));
        $this->command = new RegisterVehicleIntoFleet($this->fleet->id(), $this->vehicle->plateNumber());
    }

    public function testHandleFleetNotFound()
    {
        $this->fleetRepository
            ->expects($this->once())
            ->method('findById')->with($this->fleet->id())->willReturn(null)
        ;

        $this->expectException(FleetNotFoundException::class);
        $this->expectExceptionMessage('Couldn\'t find fleet with id: ' . $this->fleet->id()->toNative());
        $this->registerVehicleIntoFleetHandler->handle($this->command);
    }

    public function testSuccessfulHandleWithUnknownVehicle()
    {
        $this->fleetRepository
            ->expects($this->once())
            ->method('findById')->willReturn($this->fleet)
        ;

        $this->vehicleRepository
            ->expects($this->once())
            ->method('findByPlateNumber')->willReturn(null)
        ;

        $this->fleet
            ->expects($this->once())
            ->method('registerVehicle')->with(
                $this->callback(fn($vehicle) => $vehicle->plateNumber() === $this->vehicle->plateNumber())
            )
        ;

        $this->fleetRepository
            ->expects($this->once())
            ->method('update')->with($this->fleet)
        ;

        $this->registerVehicleIntoFleetHandler->handle($this->command);
    }

    public function testSuccessfulHandleWithExistingVehicle()
    {
        $this->fleetRepository
            ->expects($this->once())
            ->method('findById')->willReturn($this->fleet)
        ;

        $this->vehicleRepository
            ->expects($this->once())
            ->method('findByPlateNumber')->willReturn($this->vehicle)
        ;

        $this->vehicleRepository
            ->expects($this->never())
            ->method('create')
        ;

        $this->fleetRepository
            ->expects($this->once())
            ->method('update')->with($this->fleet)
        ;

        $this->registerVehicleIntoFleetHandler->handle($this->command);
    }
}
