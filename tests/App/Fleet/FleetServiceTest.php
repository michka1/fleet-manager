<?php

namespace App\Fleet;

use Fulll\App\Contracts\MessageBusInterface;
use Fulll\App\Fleet\Command\CreateFleet;
use Fulll\App\Fleet\Command\ParkVehicle;
use Fulll\App\Fleet\Command\RegisterVehicleIntoFleet;
use Fulll\App\Fleet\FleetService;
use Fulll\Domain\Fleet\Characteristics\FleetId;
use Fulll\Domain\Fleet\Characteristics\UserId;
use Fulll\Domain\Fleet\Fleet;
use Fulll\Domain\Fleet\Repository\FleetRepository;
use Fulll\Domain\Vehicle\Characteristics\Location;
use Fulll\Domain\Vehicle\Characteristics\PlateNumber;
use Fulll\Domain\Vehicle\Repository\VehicleRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class FleetServiceTest extends TestCase
{
    private MockObject $commandBus;
    private MockObject $fleetRepository;
    private MockObject $vehicleRepository;
    private FleetService $fleetService;

    protected function setUp(): void
    {
        $this->commandBus = $this->createMock(MessageBusInterface::class);
        $this->fleetRepository = $this->createMock(FleetRepository::class);
        $this->vehicleRepository = $this->createMock(VehicleRepository::class);

        $this->fleetService = new FleetService($this->commandBus, $this->fleetRepository, $this->vehicleRepository);
    }

    public function testCreateFleet()
    {
        $name = 'The best shiny fleet in ze world';

        $userId = UserId::generate();

        $expectedFleet = new Fleet(FleetId::generate(), $userId, $name);

        $this->commandBus
            ->expects($this->once())
            ->method('send')->with($this->equalTo(new CreateFleet($userId, $name)))->willReturn($expectedFleet)
        ;

        $fleetId = $this->fleetService->createFleet($userId->toNative(), $name);

        $this->assertEquals($expectedFleet->id()->toNative(), $fleetId);
        $this->assertEquals($expectedFleet->userId(), $userId);
    }

    public function testRegisterVehicle()
    {
        $fleetId = FleetId::generate();

        $nativePlateNumber = 'AB-123-CD';

        $this->commandBus
            ->expects($this->once())
            ->method('send')->with($this->isInstanceOf(RegisterVehicleIntoFleet::class))
        ;

        $this->fleetService->registerVehicleIntoFleet($fleetId->toNative(), $nativePlateNumber);
    }

    public function testParkVehicleToLocationSuccess()
    {
        $fleetId = FleetId::generate();
        $plateNumber = PlateNumber::fromNative('AA-123-AA');

        $latitude = 12.666666;
        $longitude = 55.458798;
        $location = Location::fromNative($latitude, $longitude);

        $this->commandBus->expects($this->once())
            ->method('send')
            ->with(
                $this->callback(fn (ParkVehicle $command) =>
                    $command->fleetId()->equals($fleetId)
                    && $command->plateNumber()->equals($plateNumber)
                    && $command->location()->equals($location)
                )
            );

        $this->fleetService->parkVehicleToLocation($fleetId->toNative(), $plateNumber->toNative(), $latitude, $longitude);
    }
}
