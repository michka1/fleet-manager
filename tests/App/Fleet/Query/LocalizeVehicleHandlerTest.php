<?php

namespace App\Fleet\Query;

use Fulll\App\Fleet\Exception\FleetNotFoundException;
use Fulll\App\Fleet\Exception\VehicleNotFoundException;
use Fulll\App\Fleet\Query\LocalizeVehicle;
use Fulll\App\Fleet\Query\LocalizeVehicleHandler;
use Fulll\Domain\Fleet\Characteristics\FleetId;
use Fulll\Domain\Fleet\Exception\VehicleDoesntBelongToFleetException;
use Fulll\Domain\Fleet\Exception\VehicleNotParkedException;
use Fulll\Domain\Fleet\Fleet;
use Fulll\Domain\Fleet\Repository\FleetRepository;
use Fulll\Domain\Vehicle\Characteristics\Location;
use Fulll\Domain\Vehicle\Characteristics\PlateNumber;
use Fulll\Domain\Vehicle\Repository\VehicleRepository;
use Fulll\Domain\Vehicle\Vehicle;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class LocalizeVehicleHandlerTest extends TestCase
{
    private MockObject $fleetRepository;
    private MockObject $vehicleRepository;
    private LocalizeVehicleHandler $handler;
    private FleetId $fleetId;
    private PlateNumber $plateNumber;
    private LocalizeVehicle $localizeVehicleQuery;
    private MockObject $fleet;

    protected function setUp(): void
    {
        $this->fleetRepository = $this->createMock(FleetRepository::class);
        $this->vehicleRepository = $this->createMock(VehicleRepository::class);
        $this->fleet = $this->createMock(Fleet::class);
        $this->vehicle = $this->createMock(Vehicle::class);

        $this->fleetId = FleetId::generate();
        $this->plateNumber = PlateNumber::fromNative('AA-123-AA');

        $this->localizeVehicleQuery = new LocalizeVehicle($this->fleetId, $this->plateNumber);
        $this->handler = new LocalizeVehicleHandler($this->fleetRepository, $this->vehicleRepository);
    }

    public function testHandleFleetNotFound()
    {
        $this->fleetRepository->method('findById')->willReturn(null);

        $this->expectException(FleetNotFoundException::class);
        $this->expectExceptionMessage('Couldn\'t find fleet with id: ' . $this->fleetId->toNative());
        $this->handler->handle($this->localizeVehicleQuery);
    }

    public function testHandleVehicleNotFound()
    {
        $this->fleetRepository->method('findById')->willReturn($this->fleet);

        $this->vehicleRepository->method('findByPlateNumber')->willReturn(null);

        $this->expectException(VehicleNotFoundException::class);
        $this->expectExceptionMessage('Couldn\'t find vehicle with plate number: ' . $this->plateNumber->toNative());
        $this->handler->handle($this->localizeVehicleQuery);
    }

    public function testHandleVehicleDoesntBelongToFleet()
    {
        $this->fleetRepository->method('findById')->willReturn($this->fleet);
        $this->vehicleRepository->method('findByPlateNUmber')->willReturn($this->vehicle);

        $this->fleet
            ->expects($this->once())
            ->method('localizeVehicle')
            ->willThrowException(new VehicleDoesntBelongToFleetException($this->plateNumber->toNative()))
        ;

        $this->expectExceptionMessage(
            'Vehicle with plate number: "'  . $this->plateNumber->toNative() . '" doesn\'t belong to your fleet'
        );
        $this->handler->handle($this->localizeVehicleQuery);
    }

    public function testHandleVehicleNotParkedException()
    {
        $this->fleetRepository->method('findById')->willReturn($this->fleet);
        $this->vehicleRepository->method('findByPlateNUmber')->willReturn($this->vehicle);

        $this->fleet
            ->expects($this->once())
            ->method('localizeVehicle')
            ->willThrowException(new VehicleNotParkedException($this->plateNumber->toNative()))
        ;

        $this->expectExceptionMessage('Vehicle ' . $this->plateNumber->toNative() . ' is not parked anywhere');
        $this->handler->handle($this->localizeVehicleQuery);
    }

    public function testHandleSuccess()
    {
        $this->fleetRepository->method('findById')->willReturn($this->fleet);
        $this->vehicleRepository->method('findByPlateNUmber')->willReturn($this->vehicle);

        $location = Location::fromNative(10, 10, 50);

        $this->fleet
            ->expects($this->once())
            ->method('localizeVehicle')
            ->willReturn($location)
        ;

        $result = $this->handler->handle($this->localizeVehicleQuery);
        $this->assertTrue($result->equals($location));
    }
}