<?php

namespace Domain\Fleet;

use Fulll\Domain\Fleet\Characteristics\FleetId;
use Fulll\Domain\Fleet\Characteristics\UserId;
use Fulll\Domain\Fleet\Exception\DuplicateVehicleException;
use Fulll\Domain\Fleet\Exception\VehicleDoesntBelongToFleetException;
use Fulll\Domain\Fleet\Exception\VehicleNotParkedException;
use Fulll\Domain\Fleet\Fleet;
use Fulll\Domain\Vehicle\Characteristics\Location;
use Fulll\Domain\Vehicle\Characteristics\PlateNumber;
use Fulll\Domain\Vehicle\Characteristics\VehicleId;
use Fulll\Domain\Vehicle\Exception\InvalidCoordinatesException;
use Fulll\Domain\Vehicle\Vehicle;
use PHPUnit\Framework\TestCase;

final class FleetTest extends TestCase
{
    private readonly Fleet $fleet;
    private readonly FleetId $fleetId;
    private readonly UserId $userId;
    private Vehicle $vehicle;
    private Location $location;

    protected function setUp(): void {
        $this->fleetId = FleetId::generate();
        $this->userId = UserId::generate();
        $this->fleet = new Fleet($this->fleetId, $this->userId);

        $this->vehicle = new Vehicle(VehicleId::generate(), PlateNumber::fromNative('AA-123-AA'));

        $this->location = new Location(89.9999999, 179.999999);
    }

    public function testGetId()
    {
        $this->assertEquals($this->fleetId, $this->fleet->id());
    }

    public function testGetUserId()
    {
        $this->assertEquals($this->userId, $this->fleet->userId());
    }

    public function testRegisterVehicle()
    {

        $this->assertFalse($this->fleet->contains($this->vehicle));

        $this->fleet->registerVehicle($this->vehicle);
        $this->assertTrue($this->fleet->contains($this->vehicle));
    }

    public function testRegisterDuplicateVehicle()
    {
        $this->fleet->registerVehicle($this->vehicle);

        $this->expectException(DuplicateVehicleException::class);
        $this->expectExceptionMessage(
            sprintf('Vehicle with plate number: "%s" already belongs to the fleet', $this->vehicle->plateNumber()->toNative()));
        $this->fleet->registerVehicle($this->vehicle);
    }

    public function testParkVehicleSuccess()
    {
        $this->fleet->registerVehicle($this->vehicle);
        $this->fleet->parkVehicle($this->vehicle, $this->location);

        $this->assertEquals($this->location, $this->vehicle->location());
    }

    public function testParkVehicleNotInFleet()
    {
        $this->expectException(VehicleDoesntBelongToFleetException::class);

        $this->fleet->parkVehicle($this->vehicle, $this->location);
    }

    public function testParkVehicleAlreadyParkedAtLocation()
    {
        $this->expectException(InvalidCoordinatesException::class);
        $this->expectExceptionMessage('Invalid coordinates provided: this vehicle was already parked to this location');

        $this->fleet->registerVehicle($this->vehicle);

        $this->fleet->parkVehicle($this->vehicle, $this->location);
        $this->fleet->parkVehicle($this->vehicle, $this->location);
    }

    public function testLocalizeVehicleDoesntBelongToFleet()
    {
        $this->expectException(VehicleDoesntBelongToFleetException::class);
        $this->fleet->localizeVehicle($this->vehicle);
    }

    public function testLocalizeVehicleNotParked()
    {
        $this->fleet->registerVehicle($this->vehicle);

        $this->expectException(VehicleNotParkedException::class);
        $this->fleet->localizeVehicle($this->vehicle);
    }

    public function testLocalizeVehicleSuccess()
    {
        $this->fleet->registerVehicle($this->vehicle);
        $this->vehicle->park($this->location);

        $result = $this->fleet->localizeVehicle($this->vehicle);
        $this->assertEquals($this->location, $result);
    }
}
