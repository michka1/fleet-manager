<?php

namespace Domain\Shared;

use Fulll\Domain\Shared\Exception\InvalidIdException;
use Fulll\Domain\Vehicle\Characteristics\VehicleId;
use PHPUnit\Framework\TestCase;

final class IdNumberTest extends TestCase
{

    public function testBadIdFormat()
    {
        $dummyId = 'dummyId';

        $this->expectException(InvalidIdException::class);
        $this->expectExceptionMessage('The provided ID: "' . $dummyId . '" has an invalid format');
        VehicleId::fromNative($dummyId);

    }
}
