<?php

namespace Domain\Vehicle\Characteristics;

use Fulll\Domain\Shared\Exception\IncompatibleForEqualityCheckException;
use Fulll\Domain\Vehicle\Characteristics\Location;
use Fulll\Domain\Vehicle\Exception\InvalidCoordinatesException;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class LocationTest extends TestCase
{
    CONST LATITUDE = 10.255447954654;
    const LONGITUDE = 5.22659821545454;
    const ALTITUDE = 2000;
    public function setUp(): void
    {
        $this->location = new Location(self::LATITUDE, self::LONGITUDE, self::ALTITUDE);
    }

    public function testGetLatitude()
    {
        $this->assertIsFloat($this->location->latitude());
        $this->assertEquals(10.255448, $this->location->latitude());
    }

    public function testGetLongitude()
    {
        $this->assertIsFloat($this->location->longitude());
        $this->assertEquals(5.226598, $this->location->longitude());
    }

    public function testGetAltitude()
    {
        $this->assertIsInt($this->location->altitude());
        $this->assertEquals(self::ALTITUDE, $this->location->altitude());
    }

    public function testEquals()
    {
        $newLocation = new Location(self::LATITUDE, self::LONGITUDE, self::ALTITUDE);
        $this->assertTrue($this->location->equals($newLocation));

        $this->expectException(IncompatibleForEqualityCheckException::class);
        $this->assertTrue($this->location->equals('not a location at all'));
    }

    #[DataProvider('wrongLatitudeProvider')]
    public function testWrongLatitude($latitude, $expectedMessage)
    {
        $this->wrongCoordinates($latitude, self::LONGITUDE, self::ALTITUDE, $expectedMessage);
    }

    public static function wrongLatitudeProvider()
    {
        return [
            [-1, 'Invalid coordinates provided: Latitude cannot be negative.'],
            [91, 'Invalid coordinates provided: Latitude cannot exceed 90.'],
        ];
    }

    #[DataProvider('wrongLongitudeProvider')]
    public function testWrongLongitude($longitude, $expectedMessage)
    {
        $this->wrongCoordinates(self::LATITUDE, $longitude, self::ALTITUDE, $expectedMessage);
    }

    public static function wrongLongitudeProvider()
    {
        return [
            [-1, 'Invalid coordinates provided: Longitude cannot be negative.'],
            [181, 'Invalid coordinates provided: Longitude cannot exceed 180.'],
        ];
    }

    #[DataProvider('wrongAltitudeProvider')]
    public function testWrongAltitude($altitude, $expectedMessage)
    {
        $this->wrongCoordinates(self::LATITUDE, self::LONGITUDE, $altitude, $expectedMessage);
    }

    public static function wrongAltitudeProvider()
    {
        return [
            [-1, 'Invalid coordinates provided: Altitude cannot be negative.'],
            [8850, 'Invalid coordinates provided: Altitude cannot exceed 8849.'],
        ];
    }

    private function wrongCoordinates(float $latitude, float $longitude, int $altitude, $exceptionMessage)
    {
        try {
            new Location($latitude, $longitude, $altitude);
            $this->fail('Missing exception');
        } catch (InvalidCoordinatesException $e) {
            $this->assertEquals($exceptionMessage, $e->getMessage());
        }
    }
}
