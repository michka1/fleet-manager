<?php

namespace Domain\Vehicle\Characteristics;

use Fulll\Domain\Shared\Exception\IncompatibleForEqualityCheckException;
use Fulll\Domain\Vehicle\Characteristics\PlateNumber;
use Fulll\Domain\Vehicle\Exception\InvalidPlateNumberException;
use PHPUnit\Framework\TestCase;

final class PlateNumberTest extends TestCase
{
    public static function wrongPlateNumberDataProvider(): array
    {
        return [
            ['dummy'],
            ['OO-123-SW'],
            ['IQ-128-DD'],
            ['AA-123-UR'],
            ['AA-1-AA'],
            // ... some cases missing because of regex pattern not perfectly designed
        ];
    }

    /**
     * @dataProvider wrongPlateNumberDataProvider
     */
    public function testWrongPlateNumberFormat($wrongPlateNumber)
    {
        $this->expectException(InvalidPlateNumberException::class);
        $this->expectExceptionMessage(
            sprintf('The given plate number seems invalid: "%s"', $wrongPlateNumber)
        );
        PlateNumber::fromNative($wrongPlateNumber);

    }

    public function testRightPlateNumberFormat()
    {
        $nativePlateNumber = 'AB-123-CD';
        $plateNumber = PlateNumber::fromNative($nativePlateNumber);
        $this->assertEquals($nativePlateNumber, $plateNumber->toNative());
    }

    public function testPlateNumbersEquality()
    {
        $nativePlateNumber = 'CC-123-DD';
        $plateNumber1 = PlateNumber::fromNative($nativePlateNumber);
        $plateNumber2 = PlateNumber::fromNative($nativePlateNumber);
        $this->assertTrue($plateNumber1->equals($plateNumber2));

        $plateNumber3 = PlateNumber::fromNative('EE-456-EE');
        $this->assertFalse($plateNumber1->equals($plateNumber3));

        $this->expectException(IncompatibleForEqualityCheckException::class);
        $plateNumber1->equals("I'm not a PlateNumber object");
    }
}
