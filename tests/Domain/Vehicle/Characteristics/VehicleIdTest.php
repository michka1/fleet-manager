<?php

namespace Domain\Vehicle\Characteristics;

use Fulll\Domain\Fleet\Characteristics\FleetId;
use Fulll\Domain\Shared\Exception\IncompatibleForEqualityCheckException;
use Fulll\Domain\Vehicle\Characteristics\VehicleId;
use PHPUnit\Framework\TestCase;

class VehicleIdTest extends TestCase
{
    public function testIncompatibleEquality()
    {
        $this->expectException(IncompatibleForEqualityCheckException::class);
        VehicleId::generate()->equals(FleetId::generate());
    }
}
