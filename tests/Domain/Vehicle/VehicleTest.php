<?php

namespace Domain\Vehicle;

use Fulll\Domain\Vehicle\Characteristics\Location;
use Fulll\Domain\Vehicle\Characteristics\PlateNumber;
use Fulll\Domain\Vehicle\Characteristics\VehicleId;
use Fulll\Domain\Vehicle\Vehicle;
use PHPUnit\Framework\TestCase;

class VehicleTest extends TestCase
{
    public function testGetId()
    {
        $vehicleId = VehicleId::generate();
        $vehicle = new Vehicle($vehicleId, PlateNumber::fromNative('AA-123-AA'));

        $this->assertEquals($vehicle->id(), $vehicleId);
    }

    public function testGetPlateNumber()
    {
        $plateNumber = PlateNumber::fromNative('AA-123-AA');
        $vehicle = new Vehicle(VehicleId::generate(), $plateNumber);

        $this->assertEquals($vehicle->plateNumber(), $plateNumber);
    }

    public function testGetLocation()
    {
        $location = Location::fromNative(5.18878, 8.159748);
        $vehicle = new Vehicle(VehicleId::generate(), PlateNumber::fromNative('AA-123-AA'), $location);

        $this->assertEquals($vehicle->location(), $location);
    }
}
